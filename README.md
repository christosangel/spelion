# SPELION


---

---
**Spelion** is a word spelling puzzle.

Using just the given 7 letters, you are called to
form **as many words as possible**.

![1.png](png/1.png){width=300}

---

## ABOUT THE GAME

### Rules

* The word that you form must include the **center letter**.
* You don't have to use all the **other letters**.
* Minimum length of the word that you create is **four letters**.
* Any given letters can be used **more than one time in a word**.
* The word list is **BY DEFAULT** contained in `/usr/share/dict/words`. **However** this is configurable, **the user can play the game using their preferd word list**.
  The script takes away words with *upper-case letters*, words with  *apostrophe* and words containing letters with *accent marks*.

![2.png](png/2.png){width=300}

---

### Points

* 4 letter words get 1 point.
* Bigger words get 1 point for every letter.
* Words containing ALL given letters, get a bonus of 7 extra points.

![4.png](png/4.png){width=300}

---

### Name

The name *SPELION* was just a made up word with letters that could form
the root *spel(l)*, while at the same time these letters could appear
in a (recursive) Spelion puzzle.
The name stuck when I found out that by pure chance Spelion was also
an anagram of the ***greek letter epsilon***.

---

### Dependencies

* As mentioned above, this script is using the word list contained in `/usr/share/dict/words`.

  If your distro doesn't include this installed, you can install the respective package (`wordlist`, `words`) using the respective command (`apt`, `pacman`).

* If you want to play the game with using another word list, you can do that editing the `$HOME/.config/spelion/spelion.config` file (See **Configure**).

* Another, much less important dependency is [lolcat](https://github.com/busyloop/lolcat).

 `lolcat` helps show the *Highscores* in **color**, and therefore more fun.

 ![3.png](png/3.png){width=300}

 To install `lolcat`

  * Debian based:

    ```
    sudo apt install lolcat
    ```

 * Arch based:

    ```
    sudo pacman -S lolcat
    ```

 * CentOS, RHEL, Fedora:

    ```
    sudo dnf install lolcat
    ```

---

### Install

```
git clone https://gitlab.com/christosangel/spelion.git && cd spelion/
```

To run the script from any directory, it has to be made executable, and then copied to `$PATH`:

```
chmod +x spelion.sh && cp spelion.sh ~/.local/bin/
```
After that, the user must run this command in order to create the necessary directories and files:

```
mkdir -p ~/.config/spelion/ && cp spelion.config hiscores.txt spelion.png ~/.config/spelion/
```

---
### Run

Just run:

```
spelion.sh
```

---

### Configure

* The solutions of each puzzle can be between 20 and 60 words by default.
  If you want to change these values, edit the values `MINIMUM_SOLUTIONS` and `MAXIMUM_SOLUTIONS` in  `$HOME/.config/spelion/spelion.config` file accordingly.

* The user can configure whether the highscores are shown in color or not.  This is done again by editing the value `HISCORE_COLOR` (yes / no) in  `$HOME/.config/spelion/spelion.config`

* By editing the `WORD_LIST` line in `$HOME/.config/spelion/spelion.config`, the user can play the game using another word list.

***Have fun!***
