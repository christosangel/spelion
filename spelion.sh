#! /bin/bash
# ┏━┓┏━┓┏━╸╻  ╻┏━┓┏┓╻
# ┗━┓┣━┛┣╸ ┃  ┃┃ ┃┃┗┫
# ┗━┛╹  ┗━╸┗━╸╹┗━┛╹ ╹
#A bash script written by Christos Angelopoulos, September 2023, under GPL v2



function show_hiscores () {
echo -e "${Y}${b}\t╻ ╻╻┏━╸╻ ╻┏━┓┏━╸┏━┓┏━┓┏━╸┏━┓\n\t┣━┫┃┃╺┓┣━┫┗━┓┃  ┃ ┃┣┳┛┣╸ ┗━┓\n\t╹ ╹╹┗━┛╹ ╹┗━┛┗━╸┗━┛╹┗╸┗━╸┗━┛${n}\n\n"

 sort -rh $HOME/.config/spelion/hiscores.txt|cat -n|head -10 |lolcat -p 3000 -a -s 40 -F 0.3

}

function check_score () {
 TENTH="$(sort -rh $HOME/.config/spelion/hiscores.txt|head -10|tail -1|awk '{print $1}')"
 if [[ "$RATIO" -gt "0" ]]
 then
  SCORELINE="$RATIO %  $POINTS pts $(date +%F)"
  echo -e "\t${C}${b}You found $FOUND words out of $TOTAL_SOLUTIONS_NUMBER\n\t${R}Success: $RATIO%  \n\t${M}Points: $POINTS \n\t${G}Level: $LEVEL!${n}\n"
  if [[ "$RATIO" -gt "$TENTH" ]]||[[ "$RATIO" -eq 100 ]]||[[ "$(cat $HOME/.config/spelion/hiscores.txt|wc -l)" -lt 10 ]]
  then
   echo "$SCORELINE">>$HOME/.config/spelion/hiscores.txt
   echo -e "${Y}${b}Gongratulations!${n}\nYou made it to the"
   show_hiscores
  fi
 echo -e "\nPress any key to return";read -sN 1 v;clear;
 fi
 }

function get_rank () {
RATIO=$(( $FOUND*100/$TOTAL_SOLUTIONS_NUMBER ))
TITLE=(' Rookie' ' Novice' ' Nice' ' Cool' ' Good' ' Scholar' ' Great' ' Expert' ' Master' ' Stellar' ' Guru' ' Wizard' ' Genius' ' BOSS')
RANK_LIM=('5' '10' '15' '21' '27' '34' '41' '48' '55' '62' '69' '76' '83' '90')
for i in {0..13}
do
 if [[ "$RATIO" -gt "${RANK_LIM[i]}" ]];then RANK="$RATIO% ${TITLE[i]}";LEVEL=${TITLE[i]};fi
done

RANK_STR="${RANK}${PAD}"
if [[ "$RATIO" -eq 100 ]]
then
 check_score
 db2="M"
 main_menu_reset
fi
}
function enter_word () {
 OLD_POINTS=$POINTS
 if [[ ${#WORD_STR} -lt 4 ]]
 then COMMENT=" Word is too small"
 elif [[ -z "$(echo "$WORD_STR"|grep "$CHAR_CENTER_LETTER")" ]]
 then COMMENT=" Invalid word, center letter missing"
 elif [[ -z "$(echo $TOTAL_SOLUTIONS|sed 's/ /\n/g'|grep  -E ^"$WORD_STR"$)" ]]
 then COMMENT=" Invalid word, not in solutions"
 elif [[ -n "$(echo $TOTAL_FOUND|sed 's/ /\n/g'|grep  -E ^"$WORD_STR"$)" ]]
 then COMMENT=" Word already found"
 else COMMENT=" Last word: $WORD_STR"
  TOTAL_FOUND="$TOTAL_FOUND $WORD_STR"
  ((FOUND++))
  FOUND_STR="Found: ${FOUND}${PAD}"
  if [ ${#WORD_STR} -eq 4 ]
  then POINTS=$(($POINTS + 1))
  else POINTS="$(($POINTS + ${#WORD_STR} * 1))"
  fi
  #check for pangram
  pangram="0";
  for i in ${CHAR_OTHER_LETTERS[@]}
  do
   if [[ -z "$(echo "$WORD_STR"|grep "$i")" ]]
   then pangram=1;
   fi
  done
  if [[ $pangram == "0" ]]
  then
   POINTS=$(($POINTS + 7))
   COMMENT="$COMMENT PANGRAM!"
  fi

 NEW_POINTS="$(($POINTS - $OLD_POINTS))"
 POINTS_STR="Points: ${POINTS}${PAD}"
 COMMENT="$COMMENT +$NEW_POINTS pts"
 fi
 WORD_STR="";PLACEHOLDER_STR="$WORD_STR""$PAD"
 COMMENT_STR="$COMMENT""$PAD"
 get_rank
}
function main_menu_reset () {

 WORDS_STR="            "
 FOUND_STR="            "
 POINTS_STR="            "
 RANK_STR="            "
}

function total_solutions () {
 TOTAL_SOLUTIONS="$(grep -v -E [ê,è,é,ë,â,à,ô,ó,ò,ú,ù,û,ü,î,ì,ï,í,ç,ö,á,ñ] "$WORD_LIST"|grep -v '[^[:lower:]]'|grep -v 'xx'|grep -v 'vii'|grep -E "^.....*$"|grep "$CHAR_CENTER_LETTER")"
 for i in "${LETTERS[@]}"
 do
  go="0"
  if [[ "$i" == "$CHAR_CENTER_LETTER"  ]];then go="1";fi
  for ii in ${CHAR_OTHER_LETTERS[@]}
  do
   if [[ "$i" == "$ii" ]];then go="1";fi;
  done
  if [[ "$go" == "0" ]]
  then
   TOTAL_SOLUTIONS="$(echo $TOTAL_SOLUTIONS|sed 's/ /\n/g'|grep -v "$i")"
  fi
 done
 TOTAL_SOLUTIONS_NUMBER="$(echo $TOTAL_SOLUTIONS|sed 's/ /\n/g'|wc -l)"
 if [[ "$TOTAL_SOLUTIONS_NUMBER" -eq "1" ]];then TOTAL_SOLUTIONS_NUMBER=0;fi
}


function print_box () {
 echo -e "${W}╭──────────────────────────────────────╮"
 echo -e "│    ${M}${b}╭───╮ ╭───╮${n}                       ${W}│\n│    ${M}${b}│ $2 │ │ $3 │${n}           ${WORDS_STR:0:12}${W}│\n│    ${M}${b}╰───╯ ╰───╯${n}                       ${W}│\n│ ${M}${b}╭───╮ ${Y}╭───╮ ${M}╭───╮${n}        ${FOUND_STR:0:12}${W}│\n│ ${M}${b}│ $4 │ ${Y}│ $1 │ ${M}│ $5 │${n}                    ${W}│\n│ ${M}${b}╰───╯ ${Y}╰───╯ ${M}╰───╯${n}        ${POINTS_STR:0:12}${W}│\n│    ${M}${b}╭───╮ ╭───╮${n}                       ${W}│\n│    ${M}${b}│ $6 │ │ $7 │${n}           ${RANK_STR:0:12}${W}│\n│    ${M}${b}╰───╯ ╰───╯${n}                       ${W}│"
  echo -e "${W}├──────────────────────────────────────┤"
  echo -e "$8"
  echo -e "${W}├──────────────────────────────────────┤"
}

function rules() {
 echo -e "Using just the given 7 letters, you are called to
form as many words as possible.
 ${M}${b}Rules${n}
- The word that you form must include the ${Y}${b}center letter${n}.
- You don't have to use all the other letters.
- Minimum length of the word that you create is ${Y}${b}four letters${n}.
- Any given letters can be used ${Y}${b}more than one time in a word${n}.
- The word list is contained in ${Y}${b}/usr/share/dict/words${n},
  minus words with capitals, minus words with a hyphen.

 ${M}${b}Points${n}
- 4 letter words get 1 point.
- Bigger words get 1 point for every letter.
- Words containing ALL given letters, get a bonus of 7 extra points.

 ${M}${b}Configure${n}
The solutions of each puzzle can be between 20 and 60 words by default.
If you want to change these values,
edit the ${Y}${b}\$HOME/.config/spelion/spelion.config${n} file accordingly.

 ${M}${b}Name${n}
The name SPELION was just a made up word with letters that could form
the root spel(l), while at the same time these letters could appear
in a (recursive) Spelion puzzle.
The name stuck when I found out that by pure chance Spelion was also
an anagram of the greek letter ${Y}${b}\"epsilon\"${n}.\n\nPress any key to return"
read -sN 1 v
clear
}

function play_spelion() {
 PAD="                                      "
 FOUND="0"
 POINTS="0"
 TITLE="Beginner"
 LEVEL="Beginner"
 RANK="0% Beginner"
 FOUND_STR="Found: ${FOUND}${PAD}"
 POINTS_STR="Points: ${POINTS}${PAD}"
 RANK_STR="${RANK}${PAD}"
 COMMENT=" Good Luck!"
 COMMENT_STR="$COMMENT"${PAD}
 TOTAL_FOUND=""
 RATIO="0"
 PLACEHOLDER_STR="$WORD_STR${PAD}"
 TOTAL_SOLUTIONS_NUMBER="0"
 #this loop ensures that no puzzle comes out with solutions between MINIMUM_SOLUTIONS and MAXIMUM_SOLUTIONS, as configured in ~/.config/spelion/spelion.config
 while [[ "$TOTAL_SOLUTIONS_NUMBER" -lt "$MINIMUM_SOLUTIONS" ]]||[[ "$TOTAL_SOLUTIONS_NUMBER" -gt "$MAXIMUM_SOLUTIONS" ]]
 do
  CL=$((0 + $RANDOM % 25))
  #echo "CL $CL"
  for i in {0..5}
  do
   OL[$i]=$((0 + $RANDOM % 25))
   #echo "OL[$i]: ${OL[$i]}"
   ii=0
   while [ $ii -le 5 ]
   do
    while [[ "$i" -ne "$ii" ]] && [[ "${OL["$i"]}" -eq "${OL["$ii"]}" ]] || [[ "${OL["$i"]}" -eq "$CL" ]]
    do
     OL[$i]=$((0 + $RANDOM % 25))
     ii=0
    done
    ((ii++))
   done
  done
  #make sure that AT LEAST TWO LETTERS ARE VOWELS
  for i in {0..1}
  do
   V[$i]=$((0 + $RANDOM % 5))
   ii=0
   while [ $ii -le 5 ]
   do
    while [[ "${VOWELS["${V["$i"]}"]}" == "${LETTERS["${OL["$ii"]}"]}" ]] || [[ "${VOWELS["${V["$i"]}"]}" == "${LETTERS["$CL"]}" ]] || [[ "${V[0]}" -eq "${V[1]}" ]]
    do
     V[$i]=$((0 + $RANDOM % 5))
     ii=0
    done
    ((ii++))
   done
  done
  for i in {0..5}
  do
   CHAR_OTHER_LETTERS[$i]="${LETTERS[${OL[$i]}]}"
  done
  CHAR_CENTER_LETTER=${LETTERS[$CL]}
  #INJECT VOWELS TO OTHER LETTER ARRAY
  CHAR_OTHER_LETTERS[${V[0]}]=${VOWELS[${V[0]}]}
  CHAR_OTHER_LETTERS[${V[1]}]=${VOWELS[${V[1]}]}

  total_solutions
 done
 WORD_STR=""
 WORDS_STR="Words: ${TOTAL_SOLUTIONS_NUMBER}${PAD}"
}

function play_menu () {
 db2="";
 while [[ $db2 != "M" ]]
 do
  print_box ${CHAR_CENTER_LETTER^^} ${CHAR_OTHER_LETTERS[@]^^} "│${n} Word: ${PLACEHOLDER_STR:0:31}""${W}│"
  echo -en "${W}│   ${Y}${b}<enter>${n}    to ${G}${b}ACCEPT word${n}          ${W}│\n│  ${Y}${b}<delete>${n}    to ${R}${b}ABORT word${n}           ${W}│\n│ ${Y}${b}<backspace>${n}  to ${R}${b}DELETE letter${n}        ${W}│\n│      ${Y}${b}S${n}       to ${C}${b}SHUFFLE letters${n}      ${W}│\n├──────────────────────────────────────┤\n│      ${Y}${b}W${n}       to show ${C}${b}ALL words${n}       ${W}│\n│      ${Y}${b}F${n}       to show ${C}${b}ALL FOUND words${n} ${W}│\n├──────────────────────────────────────┤\n│      ${Y}${b}M${n}       to go to ${G}${b}MAIN MENU${n}      ${W}│\n│      ${Y}${b}N${n}       to play  ${G}${b}NEW GAME${n}       ${W}│\n│      ${Y}${b}Q${n}       to ${R}${b}QUIT${n}                 ${W}│\n├──────────────────────────────────────┤\n│${n}${COMMENT_STR:0:38}${W}│\n╰──────────────────────────────────────╯\n${n}"
  #detect BACKSPACE, solution found https://stackoverflow.com/questions/4196161/bash-read-backspace-button-behavior-problem
  backspace=$(cat << eof
0000000 005177
0000002
eof
)

  read -sn 1 db2;
  if [[ $(echo "$db2" | od) = "$backspace" ]]&&[[ ${#WORD_STR} -gt 0 ]];then  WORD_STR="${WORD_STR::-1}";PLACEHOLDER_STR="$WORD_STR""$PAD";fi;
  case $db2 in
    "S") NEWCOL=("$(shuf -e ${CHAR_OTHER_LETTERS[@]})");CHAR_OTHER_LETTERS=("${NEWCOL[@]}");clear;
    ;;
    "M") clear;check_score;db="";unset ${OL[@]};main_menu_reset;
    ;;
    "N")clear;check_score;play_spelion; clear;
    ;;
    "Q") clear;check_score;exit;notify-send -t 5000 -i $HOME/.config/spelion/spelion.png " Exited Spelion.";
    ;;
    [a-z]) clear;WORD_STR="$WORD_STR""$db2";PLACEHOLDER_STR="$WORD_STR""$PAD";
    ;;
    "") clear;enter_word;
    ;;
    "3") clear; WORD_STR="";PLACEHOLDER_STR="$WORD_STR""$PAD";
    ;;
    "W") clear; echo -e "${Y}${b}ALL POSSIBLE WORDS ($TOTAL_SOLUTIONS_NUMBER)${n}\n\n$TOTAL_SOLUTIONS\n\n${Y}${b}Press any key to return${n}";read -sN 1 v;clear;
    ;;
    "F") clear; echo -e "${Y}${b}FOUND WORDS ($FOUND)${n}\n\n$( echo $TOTAL_FOUND|sed 's/ /\n/g')\n\nPress any key to return";read -sN 1 v;clear;
    ;;
    *)clear;
  esac
 done
}
function main_menu ()
{
db=""
while [ "$db" != "5" ]
do
 print_box L S P E I O N "${W}│ ${C}${b}Test your vocabulary and imagination${W} │"
 echo -en "│${n}Enter:                                ${W}│\n│ ${Y}${b}1${n} to ${G}${b}Play New Game${n}.                  ${W}│\n│ ${Y}${b}2${n} to ${C}${b}Read the Rules${n}.                 ${W}│\n│ ${Y}${b}3${n} to ${C}${b}Show High Scores${n}.               ${W}│\n│ ${Y}${b}4${n} to ${C}${b}Configure${n}.                      ${W}│\n│ ${Y}${b}5${n} to ${R}${b}Exit${n}.                           ${W}│\n"
 echo  -e "╰──────────────────────────────────────╯\n${n}"
 read -sN 1  db
 case $db in
  1) clear;play_spelion; play_menu;clear;
  ;;
  2) clear;rules;
  ;;
  3) clear;show_hiscores;echo -e "\nPress any key to return";read -sN 1 v;clear;
  ;;
  4) clear;eval $PREFERRED_EDITOR $HOME/.config/spelion/spelion.config;load_config;tput civis;clear;
  ;;
  5) clear;notify-send -t 5000 -i $HOME/.config/spelion/spelion.png " Exited Spelion.";
  ;;
  *)clear;echo -e "\n😕 ${Y}${b}$db${n} is an invalid key, please try again.\n"   ;
 esac
done
}
load_colors()
{
 R="\e[31m"
 G="\e[32m"
 Y="\e[33m"
 T="\e[34m"
 M="\e[35m"
 C="\e[36m"
 W="\e[38;5;242m" #Grid Color
 n="\e[m"
 b="\e[1m"
}
function load_config()
{
 WORD_LIST=$(awk '/DICTIONARY/ {print $2}' ~/.config/spelion/spelion.config)
 MINIMUM_SOLUTIONS=$(awk '/MINIMUM_SOLUTIONS/ {print $2}' ~/.config/spelion/spelion.config)
 MAXIMUM_SOLUTIONS=$(awk '/MAXIMUM_SOLUTIONS/ {print $2}' ~/.config/spelion/spelion.config)
 PREFERRED_EDITOR=$(awk '/PREFERRED_EDITOR/ {print $2}' ~/.config/spelion/spelion.config)
 [[ -z $WORD_LIST ]]&&WORD_LIST="/usr/share/dict/words"
 [[ -z $PREFERRED_EDITOR ]]&&PREFERRED_EDITOR="nano"
 [[ -z $MINIMUM_SOLUTIONS ]]&&MINIMUM_SOLUTIONS=20
 [[ -z $MAXIMUM_SOLUTIONS ]]&&MAXIMUM_SOLUTIONS=60
}
function cursor_reappear() {
tput cnorm
exit
}
#===============================================================================
clear
trap cursor_reappear HUP INT QUIT TERM EXIT ABRT
tput civis # make cursor invisible
LETTERS=('a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z')
VOWELS=('a' 'e' 'i' 'o' 'u' 'y')
load_config
load_colors
main_menu_reset
main_menu
